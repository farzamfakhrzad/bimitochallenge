
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
export const registrationSlice = createSlice({
    name: "registration",
    initialState: {
        currentStep:0,
        userInfo: {
            firstName:"",
            lastName:"",
            phoneNumber:"",
            insuranceType:"شخص ثالث",
            carType:"",
            carModel:"",
            prevInsure:"",
            driverDiscount:"",
            thirdDiscount:"",

        },
        isLoggedIn:false,
        carTypes:[],
        carModels:[],
        insuranceCompanies:[],
        thirdPartyDiscounts:[],
        driverDiscounts:[],
    },
    reducers: {
        setLoginState: (state, { payload }) => {
            state.isLoggedIn = payload;
        },
        updateUserInfo:((state, {payload}) => {
            state.userInfo = payload;
        }),
        updateCurrentStep:(((state, {payload}) => {
            state.currentStep = payload;
        })),
        updateCarTypes:(((state, {payload}) => {
            state.carTypes = payload.map((type) => {
                return {label:type.carType,value:type.carTypeID}
            });
            let carModels ={};
            for (let model of payload){
                carModels[model.carTypeID]=model.brand.map(brand => {
                    return {label:brand.name,value:brand.id}
                })
            }
            state.carModels = carModels;
        })),
        updateInsuranceCompanies:(((state, {payload}) => {
            state.insuranceCompanies = payload.map((company) => {
                return {label:company.company,value:company.id}
            });
        })),
        updateThirdDiscount:(((state, {payload}) => {
            state.thirdPartyDiscounts = payload.map((type) => {
                return {label:type.title,value:type.id}
            });
        })),
        updateDriverDiscount:(((state, {payload}) => {
            state.driverDiscounts = payload.map((type) => {
                return {label:type.title,value:type.id}
            });
        })),
    },
});
export const { setLoginState,updateUserInfo,updateCurrentStep,updateCarTypes,updateInsuranceCompanies,updateDriverDiscount,updateThirdDiscount } = registrationSlice.actions;

export const getCarTypes = createAsyncThunk(
    "registration/getCarTypes",
    async (filters = {}, { rejectWithValue, dispatch }) => {
        try {
            const response = await axios.get("/core/data/third-car-types");
            dispatch(updateCarTypes(response.data.result));
            return response.data.result
        }catch (error) {
            return rejectWithValue({success:false,message:error})
        }
    }
);
export const getInsureCompanies = createAsyncThunk(
    "registration/getInsuranceCompanies",
    async (filters = {}, { rejectWithValue, dispatch }) => {
        try {
            const response = await axios.get("/core/data/companies");
            dispatch(updateInsuranceCompanies(response.data.result));
            return response.data.result
        }catch (error) {
            return rejectWithValue({success:false,message:error})
        }
    }
);
export const getCarThirdDiscount = createAsyncThunk(
    "registration/getCarThirdDiscount",
    async (filters = {}, { rejectWithValue, dispatch }) => {
        try {
            const response = await axios.get("/core/data/car-third-discount");
            dispatch(updateThirdDiscount(response.data.result));
            return response.data.result
        }catch (error) {
            return rejectWithValue({success:false,message:error})
        }
    }
);
export const getCarDriverDiscount = createAsyncThunk(
    "registration/getCarDriverDiscount",
    async (filters = {}, { rejectWithValue, dispatch }) => {
        try {
            const response = await axios.get("/core/data/car-driver-discount");
            dispatch(updateDriverDiscount(response.data.result));
            return response.data.result
        }catch (error) {
            return rejectWithValue({success:false,message:error})
        }
    }
);


export default registrationSlice.reducer;
