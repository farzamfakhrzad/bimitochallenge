import './App.css';
import {DesignSystemContext,DesignSystem} from "./Helpers/DesignSystem";
import React,{useState} from "react";
import {
  Switch,
  Route
} from "react-router-dom";
import {BrowserRouter as Router,} from "react-router-dom";
import Wizard from "./Components/RegisterationWizard/Wizard"
import WizardContainer from "./Components/RegisterationWizard/WizardContainer";
function App() {
  const [designSystemValues] = useState(DesignSystem);
  return (
      <Router>
        <DesignSystemContext.Provider value={designSystemValues}>
          <Switch>
            <Route exact={true} path="/">
              <WizardContainer/>
            </Route>
          </Switch>
        </DesignSystemContext.Provider>
      </Router>
  );
}

export default App;
