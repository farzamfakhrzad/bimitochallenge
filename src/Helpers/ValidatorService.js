const ValidationService = {
    translationMap:{password:"رمز عبور",phoneNumber:"شماره موبایل",firstName:"نام",lastName: "نام خانوادگی"},
    messages: {},
    persianExpr:/^[\u0600-\u06FF\s]+$/,
    passwordLengthExpr:/^(?=.{5,11})/,
    upperCaseExpr:/^(?=.*[A-Z])/,
    lowerCaseExpr:/^(?=.*[a-z])/,
    emailExpr: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    required: function(field, value, param) {
        if(value.trim().length < 1) {
            if(ValidationService.messages.hasOwnProperty(field)) {
                if(ValidationService.messages[field].hasOwnProperty('required')){
                    return ValidationService.messages[field].required;
                }
            }
            return `${ValidationService.translationMap[field]}  نمی تواند خالی باشد`;
        }
        return false;
    },
    validInput:function(field,value,param){
        console.log(ValidationService.persianExpr.test(value),value)
        if(!ValidationService.persianExpr.test(value)){
            if(ValidationService.messages.hasOwnProperty(field)) {
                if(ValidationService.messages[field].hasOwnProperty('validInput')){
                    return ValidationService.messages[field].validInput;
                }
            }
            return "فقط کاراکتر های فارسی مورد قبول است"
        }
        return false;
    },
    validPassword: function(field, value, param) {
        // console.log(ValidationService.passwordLengthExpr.test(value),value)
        if(!ValidationService.passwordLengthExpr.test(value)) {
            return "رمز عبور باید بین ۴ تا ۱۰ کاراکتر باشد"
        }

        if(!ValidationService.lowerCaseExpr.test(value)){
            return "استفاده از کاراکتر لاتین کوچک در رمز عبور اجباری است"
        }
        console.log(value,!ValidationService.upperCaseExpr.test(value))
        if(!ValidationService.upperCaseExpr.test(value)) {
            return "استفاده از کاراکتر لاتین بزرگ در رمز عبور اجباری است"
        }
        if(ValidationService.messages.hasOwnProperty(field)) {
            if(ValidationService.messages[field].hasOwnProperty('validPassword')){
                return ValidationService.messages[field].validPassword;
            }
        }
        return false;
    },
    validate: function(options, messages) {
        ValidationService.messages = (messages)? messages : {};
        let errors = {}, error = [];
        Object.keys(options).forEach( field => {
            error = ValidationService.check(field, options[field].value, options[field].rules);
            if(error.length > 0) {
                errors[field] = error;
            }
        });

        return Object.keys(errors).length < 1? null : errors;
    },

    check: function(field, value, methods) {
        return Object.keys(methods).map(item => {
            let error = ValidationService[item](field, value, methods[item]);
            return(error)? error : false;
        }).filter(item => { return(item); });
    }

};

export default ValidationService;
