import React from "react";
export const DesignSystem = {

    //Colors
    primaryColor: "#25B79B",
    dangerColor:"red",
};
export const DesignSystemContext = React.createContext(DesignSystem);
