import React from "react";
import {Row,Col} from "antd"
import UserAvatar from "../GlobalComponents/UserAvatar";
import {useSelector} from "react-redux";
import { useMediaQuery } from 'react-responsive'
import HeaderLogo from "../../Assets/Icons/logo.svg"
export default function WizardHeader() {
    const isTabletOrMobile = useMediaQuery({ query: '(max-width: 768px)' })
    const userInfo = useSelector((state) => state.registration.userInfo);
    const isLoggedIn = useSelector((state) => state.registration.isLoggedIn);
    return(
        <Row type={"flex"}  justify={"space-between"}>
            <Col>
               <div>
                   <img src={HeaderLogo} alt={"logo"}/>
               </div>
            </Col>
            {isTabletOrMobile ? null :<Col><h3>
                سامانه مقایسه و خرید بیمه انلاین
            </h3></Col>}
            <Col>
                <div>
                    <UserAvatar userName={`${userInfo.firstName} ${userInfo.lastName}`} isLoggedIn={isLoggedIn}/>
                </div>
            </Col>
        </Row>
)
}
