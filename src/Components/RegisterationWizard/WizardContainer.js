import React from "react";
import { Layout } from "antd";
import WizardHeader from "./WizardHeader";
import Wizard from "./Wizard";
import { useMediaQuery } from 'react-responsive'
const { Content, Header,Footer } = Layout;
export default function WizardContainer() {
    const isMobile = useMediaQuery({ query: '(max-width: 768px)' })
    return (
            <Layout style={{ overflow: "hidden",backgroundColor:"white", }}>
                <Header style={{backgroundColor:"transparent"}}  theme={"light"}>
                    <WizardHeader/>
                </Header>
                <Content
                    style={{

                        marginRight: isMobile ? 10:100,
                        marginTop:isMobile ? 0:40,
                        minHeight: "85vh",
                        overflow: "hidden"
                    }}
                >
                    <Wizard/>
                </Content>
            </Layout>
    );
}
