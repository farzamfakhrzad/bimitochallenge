import React,{useState} from "react";
import {Row,Col} from "antd";
import GreenCar from "../../Assets/Icons/car-green.svg"
import UserSignUp from "./WizardSteps/UserSignUp";
import {useMediaQuery} from "react-responsive";
import {useSelector} from "react-redux";
import CarSelection from "./WizardSteps/CarSelection";
import InsuranceSelection from "./WizardSteps/InsuranceSelection";
import PrevInsurance from "./WizardSteps/PrevInsurance";
import DiscountSelection from "./WizardSteps/DiscountSelection";
function Wizard() {
    const wizardStepMap = {0:"ثبت نام",1:"انتخاب بیمه",2:"شخص ثالث",3:"شخص ثالث"};
    const isMobile = useMediaQuery({ query: '(max-width: 768px)' });
    const currentStep = useSelector((state => state.registration.currentStep));
    const userInfo = useSelector((state => state.registration.userInfo));
    const registerStep = <div>
        <UserSignUp currentStep={currentStep}/>
    </div>;
    const carSelectionStep = <div>
        <CarSelection userInfo={userInfo} currentStep={currentStep}/>
    </div>;
    const insuranceSelection = <div>
        <InsuranceSelection currentStep={currentStep}/>
    </div>;
    const  prevInsurance = <div>
        <PrevInsurance userInfo={userInfo} currentStep={currentStep}/>
    </div>;
    const discountSelection = <div>
        <DiscountSelection/>
    </div>;

    function renderWizardStep(){
        switch (currentStep) {
            case 0:
                return registerStep;
            case 1:
                return insuranceSelection;
            case 2:
                return carSelectionStep;
            case 3:
                return prevInsurance;
            case 4:
                return discountSelection;
            default:
                break;

        }
    }
    return (
        <Row>
            <Col md={{span:10}} xs={{span:23}}>
                <div>
                    <div style={{textAlign:isMobile ? "center" : ""}}>
                        <h1>{wizardStepMap[currentStep]}</h1>
                    </div>
                    {renderWizardStep()}
                </div>
            </Col>
            <Col style={{marginTop:isMobile ? "45%" : "15%"}}   md={{span:14}} xs={{span:15}}>
                <Row  justify={"center"} align={"middle"}>
                    <Col>
                        <img style={{width:isMobile ? "150%" : "100%",zIndex:-1}} src={GreenCar}/>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
}

export default Wizard;
