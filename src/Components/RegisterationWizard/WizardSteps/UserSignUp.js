import React,{useState,useContext} from "react";
import {Row,Col,Input} from "antd";
import ValidationService from "../../../Helpers/ValidatorService";
import { DesignSystemContext } from "../../../Helpers/DesignSystem";
import BimInput from "../../GlobalComponents/BimInput";
import BimButton from "../../GlobalComponents/BimButton";
import {useMediaQuery} from "react-responsive";
import {setLoginState,updateUserInfo,updateCurrentStep} from "../../../Features/RegisterationAndInsurance/Slice";
import {useDispatch} from "react-redux";
export default function UserSignUp({currentStep}) {
    const dispatch = useDispatch();
    const isMobile = useMediaQuery({ query: '(max-width: 768px)' });
    const designSystem = useContext(DesignSystemContext);
    const [firstName,setFirstName] = useState("");
    const [lastName,setLastName] = useState("");
    const [phoneNumber,setPhoneNumber] = useState("");
    const [password,setPassword] = useState("");
    const [formErrors,setFormErrors] = useState({});
    function postUserInfo(event) {
        event.preventDefault();
        let response = ValidationService.validate({
            firstName: { value: firstName, rules: {'required': true,'validInput':true} },
            lastName:{ value: lastName, rules: {'required': true, 'validInput': true } },
            phoneNumber: { value: phoneNumber, rules: {'required': true }},
            password: { value: password, rules: {'required': true,'validPassword':true }}
        });
        setFormErrors((response)? response : {});

        if(!response) {
            dispatch(updateUserInfo({firstName,lastName,phoneNumber,password}));
            dispatch(setLoginState(true));
            dispatch(updateCurrentStep(currentStep + 1))
        }
    }
return(
    <div className="container">
        <div className="col-md-6 m-auto">
            <form onSubmit={postUserInfo} method="post">
                <Row gutter={20}>
                    <Col style={{minHeight:20}} xs={{span:24}} md={{span:12}}>
                        <BimInput  type="text" name="firstName" onChange={(event => setFirstName(event.target.value))}
                               placeholder="نام"/>
                        <small style={{color:designSystem.dangerColor,minHeight:15,display:"block"}}>{(formErrors.hasOwnProperty('firstName'))? formErrors.firstName[0] : ''}</small>
                    </Col>
                    <Col style={{minHeight:20}} xs={{span:24}} md={{span:12}}>
                        <BimInput  type="text" name="lastName" onKeyUp={(event => setLastName(event.target.value))}
                                placeholder="نام خوانوادگی"/>
                        <small style={{color:designSystem.dangerColor,minHeight:20,display:"block"}}>{(formErrors.hasOwnProperty('lastName'))? formErrors.lastName[0] : ''}</small>
                    </Col>
                </Row>
                <Row >
                    <Col style={{minHeight:20}} span={24}>
                        <BimInput  type="number" name="phoneNumber" onKeyUp={(event => setPhoneNumber(event.target.value))}
                                placeholder="شماره موبایل"/>
                        <small style={{color:designSystem.dangerColor,minHeight:20,display:"block"}}>{(formErrors.hasOwnProperty('phoneNumber'))? formErrors.phoneNumber[0] : ''}</small>
                    </Col>
                </Row>
                <Row >
                    <Col style={{minHeight:20}} span={24}>
                        <BimInput  type="password" name="phoneNumber" onKeyUp={(event => setPassword(event.target.value))}
                                placeholder="رمز عبور"/>
                        <small style={{color:designSystem.dangerColor,minHeight:20,display:"block"}}>{(formErrors.hasOwnProperty('password'))? formErrors.password[0] : ''}</small>
                    </Col>
                </Row>

                <Row justify={isMobile ? "center" : "end"}>
                    <Col>
                        <BimButton style={{marginTop:30}} type="submit">ثبت نام</BimButton>
                    </Col>
                </Row>
            </form>
        </div>
    </div>
)
}
