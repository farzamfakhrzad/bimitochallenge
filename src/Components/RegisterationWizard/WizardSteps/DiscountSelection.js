import React, { useEffect, useState} from "react";
import {Col, Row ,Modal,Statistic,Card} from "antd";
import BimSelect from "../../GlobalComponents/BimSelect";
import BimButton from "../../GlobalComponents/BimButton";
import {getCarDriverDiscount,getCarThirdDiscount,updateUserInfo} from "../../../Features/RegisterationAndInsurance/Slice";
import {useDispatch, useSelector} from "react-redux";
import {useMediaQuery} from "react-responsive";
export default function DiscountSelection(){
    const isMobile = useMediaQuery({ query: '(max-width: 768px)' });
    const dispatch = useDispatch();
    const thirdPartyDiscounts = useSelector((state => state.registration.thirdPartyDiscounts));
    const userInfo = useSelector((state => state.registration.userInfo));
    const driverDiscounts = useSelector((state => state.registration.driverDiscounts));
    const [isModalVisible, setIsModalVisible] = useState(false);
    const  [selectedThirdPartDiscount,setSelectedThirdPartyDiscount] = useState({});
    const  [selectedDriverDiscount,setSelectedDriverDiscount] = useState({});
    useEffect(() => {
        dispatch(getCarThirdDiscount());
        dispatch(getCarDriverDiscount());
    },[]);

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };
    function handleCompletion() {
        dispatch(updateUserInfo({thirdDiscount:selectedThirdPartDiscount.label,driverDiscount:selectedDriverDiscount.label,...userInfo}))
        setIsModalVisible(true);
    }
    return(
        <div style={{marginTop:25}}>
            <Row>
                <Col>
                    <p style={{fontSize:15,color:"rgba(0,0,0,.65)"}}>درصد تخفیف بیمه شخص ثالث و حوادث راننده را وارد کنید.</p>
                </Col>
            </Row>
            <Row>
                <Col span={24}>
                    <BimSelect onChange={(id) => setSelectedThirdPartyDiscount(id)} options={thirdPartyDiscounts} placeholder={"درصد تخفیف ثالث"}/>
                </Col>
                <Col span={24}>
                    <BimSelect onChange={(id) => setSelectedDriverDiscount(id)} options={driverDiscounts} placeholder={"درصد تخفیف حوادث راننده"}/>
                </Col>
            </Row>
            <Row gutter={20} justify={"end"}>
                <Col>
                    <BimButton onClick={()=> handleCompletion()} style={{fontSize:isMobile ? 10 : 15}}>

                                استعلام قیمت

                    </BimButton>
                </Col>
            </Row>
            <Modal title="نتیجه استعلام" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} footer={[<BimButton onClick={() => handleOk()} style={{fontSize:isMobile ? 12 : 15}}>
                متوجه شدم
            </BimButton>]}>
                <Row gutter={[10,10]} align={"middle"} justify={"center"}>
                    <Col>
                        <Card style={{textAlign:"center"}} bordered={false} title="نام و نام خوانوادگی" >
                            {`${userInfo.firstName} ${userInfo.lastName}`}
                        </Card>
                    </Col>
                    <Col>
                        <Card style={{textAlign:"center"}} bordered={false} title="شماره موبایل" >
                            {userInfo.phoneNumber}
                        </Card>
                    </Col>
                    <Col>
                        <Card title={"نوع بیمه"} style={{textAlign:"center"}} bordered={false} >
                            شخص ثالث
                        </Card>
                    </Col>
                </Row>
                <Row gutter={[10,10]} align={"middle"} justify={"center"}>
                    <Col>
                        <Card style={{textAlign:"center"}} bordered={false} title="نوع خودرو">
                            {userInfo.carType}
                        </Card>
                    </Col>
                    <Col>
                        <Card style={{textAlign:"center"}} bordered={false} title="مدل خودرو" >
                            {userInfo.carModel}
                        </Card>
                    </Col>
                    <Col>
                        <Card style={{textAlign:"center"}} bordered={false} title="شرکت بیمه گر قبلی" >
                            {userInfo.prevInsure}
                        </Card>
                    </Col>
                    <Col>
                        <Card style={{textAlign:"center"}} bordered={false} title="درصد تخفیف ثالث">
                            {userInfo.thirdDiscount}
                        </Card>
                    </Col>
                    <Col>
                        <Card style={{textAlign:"center"}} bordered={false} title="درصد تخفیف حوادث راننده">
                            {userInfo.driverDiscount}
                        </Card>
                    </Col>
                </Row>
            </Modal>
        </div>
    )
}
