import React, {useContext, useEffect, useState} from "react";
import {Col, Row} from "antd";
import BimButton from "../../GlobalComponents/BimButton";
import {updateCurrentStep, getInsureCompanies, updateUserInfo} from "../../../Features/RegisterationAndInsurance/Slice";
import Arrow from "../../../Assets/Icons/arrow.svg";
import {useMediaQuery} from "react-responsive";
import {DesignSystemContext} from "../../../Helpers/DesignSystem";
import {useDispatch, useSelector} from "react-redux";
import BimSelect from "../../GlobalComponents/BimSelect";
export default function PrevInsurance({currentStep,userInfo}) {
    const isMobile = useMediaQuery({ query: '(max-width: 768px)' });
    const designSystem = useContext(DesignSystemContext);
    const dispatch = useDispatch();
    const insuranceCompanies = useSelector((state => state.registration.insuranceCompanies));
    const  [selectedPrevInsurance,setSelectedPrevInsurance] = useState({});
    useEffect(() => {
        dispatch(getInsureCompanies())
    },[]);
    function handleCarSelectionCompletion(){
        dispatch(updateUserInfo({prevInsure:selectedPrevInsurance.label,...userInfo}));
        dispatch(updateCurrentStep(currentStep+1));
    }
    return(
        <div style={{marginTop:25}}>
            <Row>
                <Col>
                    <p style={{fontSize:15,color:"rgba(0,0,0,.65)"}}>نوع و مدل خودروی خود را انتخاب کنید</p>
                </Col>
            </Row>
            <Row>
                <Col span={24}>
                    <BimSelect onChange={(id) => setSelectedPrevInsurance(id)} options={insuranceCompanies} placeholder={"شرکت بیمه گر قبلی خود"}/>
                </Col>
            </Row>
                <Row gutter={20} justify={"space-between"}>
                    <Col>
                        <BimButton onClick={()=> dispatch(updateCurrentStep(currentStep-1))} style={{border:`1px solid ${designSystem.primaryColor}`,backgroundColor:"transparent",color:designSystem.primaryColor,fontSize:isMobile ? 10 : 15}}>
                            <Row justify={"center"} align={"middle"}>
                                <Col pull={10}>
                                    <img style={{width:15,transform: "rotate(180deg)"}} src={Arrow} alt={""}/>
                                </Col>
                                <Col>
                                    مرحله قبل
                                </Col>
                            </Row>
                        </BimButton>
                    </Col>
                    <Col>
                        <BimButton  disabled={!selectedPrevInsurance.label} onClick={()=> handleCarSelectionCompletion()} style={{cursor: !selectedPrevInsurance.label   ? "not-allowed" : "",border:`1px solid ${designSystem.primaryColor}`,backgroundColor:"transparent",color:designSystem.primaryColor,fontSize:isMobile ? 10 : 15}} >
                            <Row justify={"center"} align={"middle"}>
                                <Col>
                                    <span>مرحله بعد</span>
                                </Col>
                                <Col push={7}>
                                    <img style={{width:15}} src={Arrow} alt={""}/>
                                </Col>
                            </Row>
                        </BimButton>
                    </Col>
                </Row>
        </div>)
}
