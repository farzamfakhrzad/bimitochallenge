import React from "react";
import {Col,Row} from "antd";
import BimTileButton from "../../GlobalComponents/BimTileButton";
import InsuranceLogo from "../../../Assets/Icons/insurance.svg"
import {useDispatch} from "react-redux";
import {updateCurrentStep} from "../../../Features/RegisterationAndInsurance/Slice";
import {useMediaQuery} from "react-responsive";

export default function InsuranceSelection({currentStep}) {
    const isMobile = useMediaQuery({ query: '(max-width: 768px)' });
    const insuranceTypes = [{title:"شخص ثالث",cover:InsuranceLogo,active:true,id:0},{title: "بیمه بدنه",cover: InsuranceLogo,active: false,id:1}];
    const dispatch = useDispatch();
    function handleInsuranceSelection(id) {
        if (id === 0) dispatch(updateCurrentStep(currentStep + 1))
    }
return(
    <Row gutter={isMobile ? 15 : 0} style={{marginTop:isMobile ? "20%" : 0}} justify={isMobile ? "center"  : ""}>
        {insuranceTypes.map((type,index) => <Col key={index} md={{span:9}} >
            <BimTileButton onClick={() => handleInsuranceSelection(type.id)} cover={type.cover} title={type.title} active={type.active}/>
        </Col>)}
    </Row>
)
}
