import React, {useContext, useEffect, useState} from "react";
import {Row,Col} from "antd";
import BimSelect from "../../GlobalComponents/BimSelect";
import {getCarTypes,updateCurrentStep,updateUserInfo} from "../../../Features/RegisterationAndInsurance/Slice";
import {useDispatch, useSelector} from "react-redux";
import BimButton from "../../GlobalComponents/BimButton";
import Arrow from "../../../Assets/Icons/arrow.svg"
import {DesignSystemContext} from "../../../Helpers/DesignSystem";
import {useMediaQuery} from "react-responsive";
export default function CarSelection({currentStep,userInfo}) {
    const isMobile = useMediaQuery({ query: '(max-width: 768px)' });
    const designSystem = useContext(DesignSystemContext);
    const dispatch = useDispatch();
    const carTypes = useSelector((state => state.registration.carTypes));
    const carModels = useSelector((state => state.registration.carModels));
    const [selectedCarType,setSelectedCarType] = useState({});
    const [selectedCarModel,setSelectedCarModel] = useState({});

    function handleCarSelectionCompletion(){
        dispatch(updateUserInfo({carType:selectedCarType.label,carModel:selectedCarModel.label,...userInfo}));
        console.log("here");
        dispatch(updateCurrentStep(currentStep+1));
    }
    useEffect(() => {
        dispatch(getCarTypes())
    },[]);
return(
    <div style={{marginTop:25}}>
        <Row>
            <Col>
                <p style={{fontSize:15,color:"rgba(0,0,0,.65)"}}>نوع و مدل خودروی خود را انتخاب کنید</p>
            </Col>
        </Row>
    <Row gutter={20}>
        <Col xs={{span:24}} md={{span:12}}>
            <BimSelect onChange={(id) => setSelectedCarType(id)} options={carTypes} placeholder={"نوع خودرو"}/>
        </Col>
        <Col xs={{span:24}} md={{span:12}}>
            <BimSelect  disabled={!selectedCarType.label} onChange={(id) => setSelectedCarModel(id)} options={carModels[selectedCarType.value]} placeholder={"مدل خودرو"}/>
        </Col>
    </Row>
        <Row gutter={20} justify={"space-between"}>
            <Col>
                <BimButton onClick={()=> dispatch(updateCurrentStep(currentStep-1))} style={{border:`1px solid ${designSystem.primaryColor}`,backgroundColor:"transparent",color:designSystem.primaryColor,fontSize:isMobile ? 10 : 15}}>
                    <Row justify={"center"} align={"middle"}>
                        <Col pull={10}>
                            <img style={{width:15,transform: "rotate(180deg)"}} src={Arrow} alt={""}/>
                        </Col>
                        <Col>
                            بازگشت
                        </Col>
                    </Row>
                </BimButton>
            </Col>
            <Col>
                <BimButton disabled={!(selectedCarType.label && selectedCarModel.label)}  onClick={()=> handleCarSelectionCompletion()} style={{cursor: !(selectedCarType.label && selectedCarModel.label) ? "not-allowed" : "",border:`1px solid ${designSystem.primaryColor}`,backgroundColor:"transparent",color:designSystem.primaryColor,fontSize:isMobile ? 10 : 15}} >
                    <Row justify={"center"} align={"middle"}>
                        <Col>
                            <span>مرحله بعد</span>

                        </Col>
                        <Col push={7}>
                            <img style={{width:15}} src={Arrow} alt={""}/>
                        </Col>
                    </Row>
                </BimButton>
            </Col>
        </Row>
    </div>

)
}
