import React from "react";
import userSvg from "../../Assets/Icons/user.svg"
import {Col, Row} from "antd";
export default function UserAvatar({userName,isLoggedIn}) {
    const userProfile = <Row gutter={10}>
        <Col>
            <img style={{width:20}} src={userSvg} alt={"userLogo"}/>
        </Col>
        <Col>
            {userName}
        </Col>
    </Row>;
return(
    <div>
        {isLoggedIn ? userProfile : <p>ثبت نام</p>}
    </div>
)
}
