import React from "react";
import {Select} from "antd";

export default function BimSelect(props) {
return(
    <Select labelInValue={true} size={"large"} style={{padding:"10px 0px",fontSize:15,width:"100%"}}  {...props}/>
)
}
