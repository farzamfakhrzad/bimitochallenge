import React from "react";
import {Button,Row,Col} from "antd";

export default function BimTileButton(props) {
    return(
        <Button disabled={!props.active} style={{height:120,width:120,borderRadius:16,padding:15}}  {...props}>
            <Row justify={"center"} align={"middle"}>
                <Col>
                    <img src={props.cover} style={{height:50,margin:8}} alt={"coverImage"}/>
                    <h3 >{props.title}</h3>
                </Col>
            </Row>
        </Button>
    )
}
