import React from "react";
import styled from '@emotion/styled'
export default function BimButton(props) {
    const Button = styled.button`
  background-color: #04AA6D;
  border: none;
  color: white;
  padding: 10px 45px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  border-radius: 25px;
}
`;
    return(
        <Button  onClick={props.onClick} {...props}/>
    )
}
