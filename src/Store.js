import { configureStore } from '@reduxjs/toolkit'
import thunk from "redux-thunk"
import registrationReducer from "./Features/RegisterationAndInsurance/Slice"

export default configureStore({
    reducer: {
        registration: registrationReducer,
    },
    middleware:[thunk],
    devTools:true
})

