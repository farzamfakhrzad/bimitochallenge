import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import store from "./Store"
import { Provider } from 'react-redux'
import reportWebVitals from './reportWebVitals';
import axios from 'axios';
import { ConfigProvider } from 'antd';
console.log(process.env);
axios.defaults.baseURL = process.env.REACT_APP_BASE_URL;
ReactDOM.render(
    <Provider store={store}>
        <ConfigProvider direction="rtl">
        <App />
        </ConfigProvider>
    </Provider>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
